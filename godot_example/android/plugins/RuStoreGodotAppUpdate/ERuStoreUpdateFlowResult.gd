# ERuStoreUpdateFlowResult
# @brief Информация о результате обновления.
class_name ERuStoreUpdateFlowResult extends Object

# @brief Доступные значения.
enum Item {
	# @brief
	#	Обновление выполнено,
	#	код может не быть получен, т. к. приложение в момент обновления завершается.
	RESULT_OK = -1,
	
	# @brief
	#	Флоу прервано пользователем, или произошла ошибка.
	#	Предполагается, что при получении этого кода следует завершить работу приложения.
	RESULT_CANCELED = 0,
	
	# @brief
	#	RuStore не установлен, либо установлена версия,
	#	которая не поддерживает принудительное обновление (RuStore versionCode < 191).
	ACTIVITY_NOT_FOUND = 2
}
