# ERuStoreInstallErrorCode
# @brief Код ошибки во время скачивания обновления.
class_name ERuStoreInstallErrorCode extends Object

# @brief Доступные значения.
enum Item {
	# @brief Неизвестная ошибка.
	ERROR_UNKNOWN = 4001,
	
	# @brief Ошибка при скачивании.
	ERROR_DOWNLOAD = 4002,
	
	# @brief Установка заблокированна системой.
	ERROR_BLOCKED = 4003,
	
	# @brief Некорректный APK обновления.
	ERROR_INVALID_APK = 4004,
	
	# @brief Конфликт с текущей версией приложения.
	ERROR_CONFLICT = 4005,
	
	# @brief Недостаточно памяти на устройстве.
	ERROR_STORAGE = 4006,
	
	# @brief Несовместимо с устройством.
	ERROR_INCOMPATIBLE = 4007,
	
	# @brief Приложение не куплено.
	ERROR_APP_NOT_OWNED = 4008,
	
	# @brief Внутренняя ошибка.
	ERROR_INTERNAL_ERROR = 4009,
	
	# @brief Пользователь отказался от установки обновления.
	ERROR_ABORTED = 4010,
	
	# @brief APK для запуска установки не найден.
	ERROR_APK_NOT_FOUND = 4011,
	
	# @brief
	#	Запуск обновления запрещён.
	#	Например, в первом методе вернулся ответ о том, что обновление недоступно,
	#	но пользователь вызывает второй метод.
	ERROR_EXTERNAL_SOURCE_DENIED = 4012,
	
	# @brief Ошибка отправки intent на открытие активити.
	ERROR_ACTIVITY_SEND_INTENT = 9901,
	
	# @brief Неизвестная ошибка отрытия активити.
	ERROR_ACTIVITY_UNKNOWN = 9902
}
