# RuStoreAppUpdateInfo
# @brief Информция о доступном обновлении.
class_name RuStoreAppUpdateInfo extends Object

# @brief Доступность обновления.
var updateAvailability: ERuStoreUpdateAvailability.Item = ERuStoreUpdateAvailability.Item.UNKNOWN

# @brief Статус установки обновления, если пользователь уже устанавливает обновление в текущий момент времени.
var installStatus: ERuStoreInstallStatus.Item = ERuStoreInstallStatus.Item.UNKNOWN

# @brief Код версии обновления.
var availableVersionCode: int = 0


func _init(json: String = ""):
	if json != "":
		var obj = JSON.parse_string(json)
		updateAvailability = int(obj["updateAvailability"])
		installStatus = int(obj["installStatus"])
		availableVersionCode = int(obj["availableVersionCode"])
