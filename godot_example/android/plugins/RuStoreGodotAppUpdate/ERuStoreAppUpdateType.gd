class_name ERuStoreAppUpdateType extends Object

enum Item {
	UNKNOWN,
	FLEXIBLE,
	IMMEDIATE,
	SILENT
}
