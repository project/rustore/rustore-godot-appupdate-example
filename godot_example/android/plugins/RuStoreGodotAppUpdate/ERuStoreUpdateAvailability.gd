# ERuStoreUpdateAvailability
# @brief Доступность обновления.
class_name ERuStoreUpdateAvailability extends Object

# @brief Доступные значения.
enum Item {
	# @brief Значение по умолчанию.
	UNKNOWN,
	
	# @brief Обновление не требуется.
	UPDATE_NOT_AVAILABLE,
	
	# @brief Обновление требуется загрузить или обновление уже загружено на устройство пользователя.
	UPDATE_AVAILABLE,
	
	# @brief Обновление уже скачивается или установка уже запущена.
	DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS
}
