# RuStoreInstallState
# @brief Описывает текущее состояние установки обновления.
class_name RuStoreInstallState extends Object

# @brief Количество загруженных байт.
var bytesDownloaded: int = 0

# @brief Общее количество байт, которое необходимо скачать.
var totalBytesToDownload: int = 0

# @brief Процент загрузки.
var percentDownloaded: float = 0

# @brief Статус установки обновления, если пользователь уже устанавливает обновление в текущий момент времени.
var installStatus: ERuStoreInstallStatus.Item = ERuStoreInstallStatus.Item.UNKNOWN

# @brief Код ошибки во время скачивания обновления.
var installErrorCode: ERuStoreInstallErrorCode.Item = ERuStoreInstallErrorCode.Item.ERROR_UNKNOWN


func _init(json: String = ""):
	if json != "":
		var obj = JSON.parse_string(json)
		bytesDownloaded = int(obj["bytesDownloaded"])
		totalBytesToDownload = int(obj["totalBytesToDownload"])
		installStatus = int(obj["installStatus"])
		installErrorCode = int(obj["installErrorCode"])
		if totalBytesToDownload > 0:
			percentDownloaded = float(bytesDownloaded) / totalBytesToDownload * 100
		else:
			percentDownloaded = 0
		if installStatus == ERuStoreInstallStatus.Item.DOWNLOADED:
			percentDownloaded = 100
