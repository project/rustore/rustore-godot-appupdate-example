# ERuStoreInstallStatus
# @brief
#	Статус установки обновления,
#	если пользователь уже устанавливает обновление в текущий момент времени.
class_name ERuStoreInstallStatus extends Object

# @brief Доступные значения.
enum Item {
	# @brief Значение по умолчанию.
	UNKNOWN,
	
	# @brief Скачано.
	DOWNLOADED,
	
	# @brief Скачивается.
	DOWNLOADING,
	
	# @brief Ошибка.
	FAILED,
	
	# @brief Установка.
	INSTALLING,
	
	# @brief В ожидании.
	PENDING
}
