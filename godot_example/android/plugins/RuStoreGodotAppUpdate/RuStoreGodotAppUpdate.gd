# RuStoreGodotAppUpdateManager
# @brief
#	Класс реализует API для трех способов обновлений.
#	В настоящий момент поддерживаются: отложенное, тихое (без UI от RuStore) и принудительное обновление.
extends Node
class_name RuStoreGodotAppUpdateManager

const SINGLETON_NAME = "RuStoreGodotAppUpdate"
const METRIC_TYPE = "godot"

# @brief Действие, выполняемое при успешном завершении get_app_update_info.
signal on_get_app_update_info_success

# @brief Действие, выполняемое в случае ошибки get_app_update_info.
signal on_get_app_update_info_failure

# @brief Действие, выполняемое при успешном завершении start_update_flow_*.
signal on_start_update_flow_success

# @brief Действие, выполняемое в случае ошибки start_update_flow_*.
signal on_start_update_flow_failure

# @brief Действие, выполняемое в случае ошибки complete_update_*.
signal on_complete_update_failure

# @brief Колбэк состояния обновления.
signal on_state_updated

var _isInitialized: bool = false
var _clientWrapper: Object = null

static var _instance: RuStoreGodotAppUpdateManager = null

# @brief
#	Получить экземпляр RuStoreGodotAppUpdateManager.
# @return
#	Возвращает указатель на единственный экземпляр RuStoreGodotAppUpdateManager (реализация паттерна Singleton).
#	Если экземпляр еще не создан, создает его.
static func get_instance() -> RuStoreGodotAppUpdateManager:
	if _instance == null:
		_instance = RuStoreGodotAppUpdateManager.new()
	return _instance


func _init():
	if Engine.has_singleton(SINGLETON_NAME):
		_clientWrapper = Engine.get_singleton(SINGLETON_NAME)
		_clientWrapper.rustore_get_app_update_info_success.connect(_on_get_app_update_info_success)
		_clientWrapper.rustore_get_app_update_info_failure.connect(_on_get_app_update_info_failure)
		_clientWrapper.rustore_start_update_flow_success.connect(_on_start_update_flow_success)
		_clientWrapper.rustore_start_update_flow_failure.connect(_on_start_update_flow_failure)
		_clientWrapper.rustore_complete_update_failure.connect(_on_complete_update_failure)
		_clientWrapper.rustore_on_state_updated.connect(_on_state_updated)
		_clientWrapper.init(METRIC_TYPE)
		_isInitialized = true


# @brief Выполняет регистрацию слушателя статуса скачивания обновления.
# @return Возвращает true, если регистрация успешна, в противном случае — false.
func register_listener() -> bool:
	return _clientWrapper.registerListener()


# @brief Если необходимости в слушателе больше нет, воспользуйтесь методом удаления слушателя.
# @return Возвращает true, если операция выполнена успешна, в противном случае — false.
func unregister_listener() -> bool:
	return _clientWrapper.unregisterListener()


# Get appUpdateInfo
# @brief Выполняет проверку наличия обновлений.
func get_app_update_info():
	_clientWrapper.getAppUpdateInfo()


func _on_get_app_update_info_success(data: String):
	var obj = RuStoreAppUpdateInfo.new(data)
	on_get_app_update_info_success.emit(obj)


func _on_get_app_update_info_failure(data: String):
	var obj = RuStoreError.new(data)
	on_get_app_update_info_failure.emit(obj)


# Check is immediate update allowed
# @brief Выполняет проверку доступности принудительного обновления.
# @return Возвращает true, если принудительное обновление доступно, в противном случае — false.
func check_is_immediate_update_allowed() -> bool:
	return _clientWrapper.checkIsImmediateUpdateAllowed()


# Start update flow
# @brief Запускает процедуру скачивания обновления приложения.
func start_update_flow_immediate():
	_clientWrapper.startUpdateFlowImmediate()

func start_update_flow_silent():
	_clientWrapper.startUpdateFlowSilent()

func start_update_flow_delayed():
	_clientWrapper.startUpdateFlowDelayed()

func _on_start_update_flow_success(resultCode: int):
	var flowResult: ERuStoreUpdateFlowResult.Item = resultCode
	on_start_update_flow_success.emit(flowResult)


func _on_start_update_flow_failure(data: String):
	var obj = RuStoreError.new(data)
	on_start_update_flow_failure.emit(obj)


# Complete update
# @brief
#	Запускает процедуру установки обновления.
#	В метод можно передавать только два типа завершения установки
#	ERuStoreAppUpdateType.Item.FLEXIBLE и ERuStoreAppUpdateType.Item.SILENT.
func complete_update_silent():
	_clientWrapper.completeUpdateSilent()

func complete_update_flexible():
	_clientWrapper.completeUpdateFlexible()

func _on_complete_update_failure(data: String):
	var obj = RuStoreError.new(data)
	on_complete_update_failure.emit(obj)


# On state updated
func _on_state_updated(data: String):
	var obj = RuStoreInstallState.new(data)
	on_state_updated.emit(obj)
