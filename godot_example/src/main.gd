extends Node

@onready var progressBar: ProgressBar = $CanvasLayer/VBoxContainer/ProgressBar
@onready var _loading = $CanvasLayer/LoadingPanel

var _core_client: RuStoreGodotCoreUtils = null
var _appUpdate_client: RuStoreGodotAppUpdateManager = null


# Called when the node enters the scene tree for the first time.
func _ready():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	
	_appUpdate_client = RuStoreGodotAppUpdateManager.get_instance()
	_appUpdate_client.on_get_app_update_info_success.connect(_on_get_app_update_info_success)
	_appUpdate_client.on_get_app_update_info_failure.connect(_on_get_app_update_info_failure)
	_appUpdate_client.on_start_update_flow_success.connect(_on_start_update_flow_success)
	_appUpdate_client.on_start_update_flow_failure.connect(_on_start_update_flow_failure)
	_appUpdate_client.on_complete_update_failure.connect(_on_complete_update_failure)
	_appUpdate_client.on_state_updated.connect(_on_state_updated)
	
	_appUpdate_client.register_listener()


# Get app update info
func _on_get_app_update_info_pressed():
	_loading.visible = true
	_appUpdate_client.get_app_update_info()


func _on_get_app_update_info_success(response: RuStoreAppUpdateInfo):
	_loading.visible = false
	var message = ERuStoreUpdateAvailability.Item.find_key(response.updateAvailability)
	_core_client.show_toast(message)


func _on_get_app_update_info_failure(error: RuStoreError):
	_loading.visible = false
	_core_client.show_toast(error.description)


# Check is immediate update allowed
func _on_is_immediate_update_allowed_pressed():
	var result = _appUpdate_client.check_is_immediate_update_allowed()
	_core_client.show_toast(str(result))


# Start update flow immediate
func _on_start_update_flow_immediate_pressed():
	_appUpdate_client.start_update_flow_immediate()


func _on_start_update_flow_silent_pressed():
	_appUpdate_client.start_update_flow_silent()


func _on_start_update_flow_delayed_pressed():
	_appUpdate_client.start_update_flow_delayed()


func _on_start_update_flow_success(flowResult: ERuStoreUpdateFlowResult.Item):
	var message = str(ERuStoreUpdateFlowResult.Item.find_key(flowResult))
	_core_client.show_toast(message)


func _on_start_update_flow_failure(error: RuStoreError):
	_core_client.show_toast(error.description)


# Complete update
func _on_complete_update_silent_pressed():
	_appUpdate_client.complete_update_silent()

func _on_complete_update_flexible_pressed():
	_appUpdate_client.complete_update_flexible()

func _on_complete_update_failure(error: RuStoreError):
	_core_client.show_toast(error.description)


# On state updated
func _on_state_updated(installState: RuStoreInstallState):
	progressBar.value = installState.percentDownloaded
	if installState.installStatus != ERuStoreInstallStatus.Item.DOWNLOADING:
		var message = str(ERuStoreInstallStatus.Item.find_key(installState.installStatus))
		_core_client.show_toast(message)
