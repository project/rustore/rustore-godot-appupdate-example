## История изменений

### Release 8.0.0
- Версия SDK appUpdate 8.0.0.

### Release 7.0.0
- Версия SDK appUpdate 7.0.0.

### Release 6.1.0
- Версия SDK appUpdate 6.1.0.

### Release 6.0.0
- Версия SDK appUpdate 6.+.

### Release 3.0.0
- Версия SDK appUpdate 3.+.
- В класс ошибки добавлено поле `name`.
- Обновлены методы установки обновления.

### Release 2.0.0
- Версия SDK appUpdate 2.+.
- Версия Godot 4.+.

### Release 1.0.1
- Версия SDK appUpdate 1.0.1.
- Версия Godot 4 — 4.1.4.

### Release 1.0.0
- Версия SDK appUpdate 1.0.0.
