package ru.rustore.godot.appupdate

import android.net.Uri
import android.util.ArraySet
import com.google.gson.GsonBuilder
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin
import org.godotengine.godot.plugin.SignalInfo
import org.godotengine.godot.plugin.UsedByGodot
import ru.rustore.godot.core.JsonBuilder
import ru.rustore.godot.core.UriTypeAdapter
import ru.rustore.sdk.appupdate.listener.InstallStateUpdateListener
import ru.rustore.sdk.appupdate.manager.RuStoreAppUpdateManager
import ru.rustore.sdk.appupdate.manager.factory.RuStoreAppUpdateManagerFactory
import ru.rustore.sdk.appupdate.model.AppUpdateInfo
import ru.rustore.sdk.appupdate.model.AppUpdateOptions
import ru.rustore.sdk.appupdate.model.AppUpdateType
import ru.rustore.sdk.appupdate.model.InstallState

class RuStoreGodotAppUpdate(godot: Godot?): GodotPlugin(godot), InstallStateUpdateListener {
    private companion object {
        const val PLUGIN_NAME = "RuStoreGodotAppUpdate"
        const val CHANNEL_GET_APP_UPDATE_INFO_SUCCESS = "rustore_get_app_update_info_success"
        const val CHANNEL_GET_APP_UPDATE_INFO_FAILURE = "rustore_get_app_update_info_failure"
        const val CHANNEL_START_UPDATE_FLOW_SUCCESS = "rustore_start_update_flow_success"
        const val CHANNEL_START_UPDATE_FLOW_FAILURE = "rustore_start_update_flow_failure"
        const val CHANNEL_COMPLETE_UPDATE_FAILURE = "rustore_complete_update_failure"
        const val CHANNEL_ON_STATE_UPDATED = "rustore_on_state_updated"
    }

    override fun getPluginName(): String {
        return PLUGIN_NAME
    }

    override fun getPluginSignals(): Set<SignalInfo> {
        val signals: MutableSet<SignalInfo> = ArraySet()
        signals.add(SignalInfo(CHANNEL_GET_APP_UPDATE_INFO_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_GET_APP_UPDATE_INFO_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_START_UPDATE_FLOW_SUCCESS, Int::class.java))
        signals.add(SignalInfo(CHANNEL_START_UPDATE_FLOW_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_COMPLETE_UPDATE_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_STATE_UPDATED, String::class.java))

        return signals
    }

    private lateinit var updateManager: RuStoreAppUpdateManager
    private var appUpdateInfo: AppUpdateInfo? = null
    private var isInitialized: Boolean = false
    private val gson = GsonBuilder()
        .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
        .create()

    @UsedByGodot
    fun init(metricType: String) {
        if (isInitialized) return

        godot.getActivity()?.run {
            updateManager = RuStoreAppUpdateManagerFactory.create(
                context = application,
                internalConfig = mapOf("type" to metricType)
            )
            isInitialized = true
        }
    }

    @UsedByGodot
    fun registerListener(): Boolean {
        if (!isInitialized) return false
        updateManager.registerListener(this)

        return true
    }

    @UsedByGodot
    fun unregisterListener(): Boolean {
        if (!isInitialized) return false
        updateManager.unregisterListener(this)

        return true
    }

    @UsedByGodot
    fun getAppUpdateInfo() {
        updateManager.getAppUpdateInfo().addOnSuccessListener { result ->
            appUpdateInfo = result
            emitSignal(CHANNEL_GET_APP_UPDATE_INFO_SUCCESS, gson.toJson(appUpdateInfo))
        }.addOnFailureListener { throwable ->
            emitSignal(CHANNEL_GET_APP_UPDATE_INFO_FAILURE, JsonBuilder.toJson(throwable))
        }
    }

    @UsedByGodot
    fun checkIsImmediateUpdateAllowed(): Boolean {
        appUpdateInfo?.let {
            return it.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
        }

        return false;
    }

    @UsedByGodot
    fun startUpdateFlowImmediate() {
        startUpdateFlow(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.IMMEDIATE).build())
    }

    @UsedByGodot
    fun startUpdateFlowSilent() {
        startUpdateFlow(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.SILENT).build())
    }

    @UsedByGodot
    fun startUpdateFlowDelayed() {
        startUpdateFlow(AppUpdateOptions.Builder().build())
    }

    private fun startUpdateFlow(appUpdateOptions: AppUpdateOptions) {
        appUpdateInfo?.let {
            updateManager.startUpdateFlow(it, appUpdateOptions)
                .addOnSuccessListener { resultCode ->
                    emitSignal(CHANNEL_START_UPDATE_FLOW_SUCCESS, resultCode)
                }
                .addOnFailureListener { throwable ->
                    emitSignal(CHANNEL_START_UPDATE_FLOW_FAILURE, JsonBuilder.toJson(throwable))
                }
        }
    }

    @UsedByGodot
    fun completeUpdateSilent() {
        completeUpdate(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.SILENT).build())
    }

    @UsedByGodot
    fun completeUpdateFlexible() {
        completeUpdate(AppUpdateOptions.Builder().appUpdateType(AppUpdateType.FLEXIBLE).build())
    }

    private fun completeUpdate(appUpdateOptions: AppUpdateOptions) {
        updateManager.completeUpdate(appUpdateOptions).addOnFailureListener { throwable ->
            emitSignal(CHANNEL_COMPLETE_UPDATE_FAILURE, JsonBuilder.toJson(throwable))
        }
    }

    override fun onStateUpdated(state: InstallState) {
        emitSignal(CHANNEL_ON_STATE_UPDATED, gson.toJson(state))
    }
}
